'use strict';
/* Ответы на теоритические вопросы:
1. Метод объекта предназначен для изменения, защиты и итерации объекта.
2. Значения свойства объекта позволяется иметь абсолютно любой тип, что позволяет 
выстраивать сложные иерархии данных, все свойства имеют ключ/значение.
3. Объект относится к ссылочному типу данных, потому, что может содержать большие объемы
данных. Переменные, содержащие ссылочный тип, его значения не содержит, 
но содержит ссылку, где находятся все данные. */



function createNewUser() {
    let personName = prompt('Введите свое имя');
    let personLastName = prompt('Введите свою фамилию');
    const newUser = {
        firstName: personName,
        lastName: personLastName,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        }
    };
    return newUser;
}
const user = createNewUser();
user.getLogin();
console.log(user.getLogin());

const copyNewUser = Object.freeze(user.firstName, user.lastName);

// copyNewUser.firstName = 'qwerty'; // не меняется из-за Object.freeze




